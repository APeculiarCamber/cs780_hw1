package main

import (
	"fmt"
)

var arr = []int{9, 3, 5, 1, 8, 2}
var target = int(7)

/*
Time Complexity: O(n).

We iterate over the array twice.
For the first loop, we perform only constant time operations using hash maps (map insert).
Similarly, for the second loop, we perform only constant time operations (map find and a subtraction).
The temporal complexity is therefore O(n) with n as the length of the array.
*/
func main() {

	present_vals := make(map[int]int)

	for i := 0; i < len(arr); i++ {
		present_vals[arr[i]] = i
	}

	foundSum := false
	other_ind := 0
	for i := 0; i < len(arr); i++ {
		other_ind, foundSum = present_vals[target-arr[i]]
		if foundSum {
			fmt.Printf("%d and %d sum to the target %d. Indices %d and %d\n",
				arr[i], arr[other_ind], target, i, other_ind)
			break
		}
	}

	if !foundSum {
		fmt.Printf("There are no two elements that sum to the target %d.\n", target)
	}
}
