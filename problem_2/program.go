package main

import (
	"fmt"
	"main/student"
	"strconv"
)

type program struct {
	userIO              UserIO
	database            *Database
	lastStudent         *student.Student
	transactionExecuted int
}

// Make a new program with userio and database
func MakeProgram(userio UserIO, database *Database) program {
	return program{userio, database, nil, 0}
}

// Prompt the user for a student name, major and id to add to the database
func handleAddStudent(p *program) {

	p.userIO.print("Enter student name: ")
	name := p.userIO.input()
	p.userIO.print("Enter student id: ")
	id := p.userIO.input()
	p.userIO.print("Enter student major: ")
	major := p.userIO.input()
	p.userIO.print("Enter student age:")
	age, serr := strconv.Atoi(p.userIO.input())
	for serr != nil && age < 0 {
		p.userIO.print("Invalid age, try again: ")
		age, serr = strconv.Atoi(p.userIO.input())
	}

	p.lastStudent = p.database.AddStudent(name, id)
	p.lastStudent.Major = major
	p.lastStudent.Age = age

	p.userIO.print(fmt.Sprintf("Student %s added\n", name))

	p.transactionExecuted++
}

// Prompt the user for a student ID to search for, outputting any matching student
func handleFindStudentByID(p *program) {

	p.userIO.print("Enter student id: ")
	id := p.userIO.input()

	student := p.database.FindStudentByID(id)
	if student == nil {
		p.userIO.print("Student not found\n")
	} else {
		p.userIO.print(student.ToString() + "\n")
		p.lastStudent = student
	}
	p.transactionExecuted++
}

// Prompt the user for a student name to search for, outputting any matching student
func handleFindStudentByName(p *program) {

	p.userIO.print("Enter student name: ")
	name := p.userIO.input()

	student := p.database.FindStudentByName(name)
	if student == nil {
		p.userIO.print("Student not found\n")
	} else {
		p.userIO.print(student.ToString() + "\n")
		p.lastStudent = student
	}
	p.transactionExecuted++
}

// Prompt the user for a course name and output all the students in that course
func handleListStudentsByCourse(p *program) {

	p.userIO.print("Enter course name: ")
	course := p.userIO.input()
	students := p.database.FindStudentsByCourse(course)

	if students == nil || len(students) == 0 {
		p.userIO.print("No student found\n")
	} else {
		// construct output string as array
		out_str := "["
		for i := 0; i < len(students)-1; i++ {
			out_str += students[i].Name + ", "
		}
		out_str += students[len(students)-1].Name + "]"

		p.userIO.print(out_str + "\n")
	}
	p.transactionExecuted++
}

// Prompt the user for a course name, its credit hours, and the grade of a student to add this course to the last student
func handleAddCourseToStudent(p *program) {

	if p.lastStudent == nil {
		p.userIO.print("Search or add student before adding a course\n")
		return
	}

	p.userIO.print("Provide course name: ")
	courseName := p.userIO.input()
	p.userIO.print("Provide credit hours: ")
	credit_hours, cerr := strconv.Atoi(p.userIO.input())
	for cerr != nil {
		p.userIO.print("Invalid credit hours, try again: ")
		credit_hours, cerr = strconv.Atoi(p.userIO.input())
	}
	p.userIO.print("Provide grade: ")
	grade, gerr := strconv.ParseFloat(p.userIO.input(), 32)
	for gerr != nil {
		p.userIO.print("Invalid grade, try again: ")
		grade, gerr = strconv.ParseFloat(p.userIO.input(), 32)
	}

	p.lastStudent.AddCourse(courseName, credit_hours, float32(grade))
	o := fmt.Sprintf("course %s added to student %s\n", courseName, p.lastStudent.Name)
	p.userIO.print(o)

	p.transactionExecuted++
}

// Output the transaction count
func handlePrintTransactionCount(p *program) {
	o := fmt.Sprintf("Transaction Count is %d\n", p.transactionExecuted)
	p.userIO.print(o)
	p.transactionExecuted++
}

// Dummy function for exitting run
func handleExit(p *program) {}

// Main Run method to allow user to configure student database
func (p *program) Run() {
	var programMenu = "Command Options:\n"
	programMenu += "- `1. Add a student`\n"
	programMenu += "- `2. Find a student by id`\n"
	programMenu += "- `3. Find a student by name`\n"
	programMenu += "- `4. List students by course`\n"
	programMenu += "- `5. Add a course to a student`\n"
	programMenu += "- `6. Print the number of transactions executed`\n"
	programMenu += "- `7. Exit`\n"

	p.userIO.print(programMenu)

forCmd:
	for {
		in := p.userIO.input()

		if cmdI, err := strconv.Atoi(in); err == nil {
			switch cmdI {
			case 1:
				handleAddStudent(p)
			case 2:
				handleFindStudentByID(p)
			case 3:
				handleFindStudentByName(p)
			case 4:
				handleListStudentsByCourse(p)
			case 5:
				handleAddCourseToStudent(p)
			case 6:
				handlePrintTransactionCount(p)
			case 7:
				p.userIO.print("Exitting...")
				break forCmd
			default:
				p.userIO.print("That is not a command number.\n")
			}
		} else {
			p.userIO.print("That is not a command number.\n")
		}
	}
}
