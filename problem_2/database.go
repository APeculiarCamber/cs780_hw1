package main

import "main/student"

/*
In the file `database.py`, create a class called `Database` that has the following attributes:

- `students` (list of `Student` objects, private)

The class should have a constructor that takes no arguments.

The `database` class should have the following methods:

- `add_student(name, id)` - add a student to the database. The function should instantiate a `Student` object and add it to the database. The function should return the student object.
- `find_student_by_id(id)` - find a student by the student's id. If the student is found, return the student object. If the student is not found, return `None`.
- `find_student_by_name(name)` - find a student by the student's name. If the student is found, return the student object. If the student is not found, return `None`. We assume that the student's name is unique.
- `find_students_by_course(course_name)` - list all students who are taking a course. The course name is a string. Return a list of student objects. The order of the list must be the same as the order that the students are added.
- `num_student()` - return the number of students in the database.
*/

type Database struct {
	students []student.Student
}

func MakeDatabase() Database {
	return Database{}
}

func (db *Database) AddStudent(name string, id string) *student.Student {
	db.students = append(db.students, student.MakeStudent(name, id))
	return &db.students[len(db.students)-1]
}

func (db *Database) FindStudentByID(id string) *student.Student {
	for i := 0; i < len(db.students); i++ {
		if db.students[i].ID == id {
			return &db.students[i]
		}
	}
	return nil
}

func (db *Database) FindStudentByName(name string) *student.Student {
	for i := 0; i < len(db.students); i++ {
		if db.students[i].Name == name {
			return &db.students[i]
		}
	}
	return nil
}

func (db *Database) FindStudentsByCourse(course_name string) []*student.Student {
	studentsInCourse := make([]*student.Student, 0)
	for i := 0; i < len(db.students); i++ {
		student := &db.students[i]
		if student.HasCourse(course_name) {
			studentsInCourse = append(studentsInCourse, student)
		}
	}
	return studentsInCourse
}

func (db *Database) NumStudents() int {
	return len(db.students)
}
