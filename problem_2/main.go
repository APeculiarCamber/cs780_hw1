package main

func main() {
	db := Database{}
	program := MakeProgram(MakeSimpleUserIO(), &db)
	program.Run()
}
