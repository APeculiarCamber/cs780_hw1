package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type SimpleUserIO struct {
	reader *bufio.Reader
}

type UserIO interface {
	input() string
	print(msg string)
}

func MakeSimpleUserIO() SimpleUserIO {
	reader := bufio.NewReader(os.Stdin)
	return SimpleUserIO{reader}
}

func (s SimpleUserIO) input() string {
	in, err := s.reader.ReadString('\n')
	if err != nil {
		return ""
	}
	return strings.TrimSpace(in)

}

func (s SimpleUserIO) print(msg string) {
	fmt.Print(msg)
}
