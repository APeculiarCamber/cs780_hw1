package student

import "fmt"

// A description of a single course for a student.
type Course struct {
	Name    string
	Credits int
	Grade   float32
}

// A description of a student (name, id, major, age, and courses)
type Student struct {
	Name    string
	ID      string
	Major   string
	Age     int
	Courses []Course
}

// Make a new student with name and id
func MakeStudent(name string, id string) Student {
	student := Student{}
	student.Name = name
	student.ID = id
	return student
}

// Add a course to the student
func (s *Student) AddCourse(courseName string, creditHours int, grade float32) {
	s.Courses = append(s.Courses, Course{courseName, creditHours, grade})
}

// Compute the students GPA
func (s *Student) CalculateGPA() float32 {
	if len(s.Courses) <= 0 {
		return 0
	}
	grade_sum := float32(0)
	credit_sum := float32(0)

	for _, course := range s.Courses {
		grade_sum += course.Grade * float32(course.Credits)
		credit_sum += float32(course.Credits)
	}
	return grade_sum / credit_sum
}

// Return true if the student has a course with name courseName
func (s *Student) HasCourse(courseName string) bool {
	for _, course := range s.Courses {
		if course.Name == courseName {
			return true
		}
	}
	return false
}

// Return a string representation of the student
func (s *Student) ToString() string {
	studentStr := "Student\n"
	studentStr += fmt.Sprintf("\tName: %s\n", s.Name)
	studentStr += fmt.Sprintf("\tID: %s\n", s.ID)
	studentStr += fmt.Sprintf("\tMajor: %s\n", s.Major)
	studentStr += fmt.Sprintf("\tAge: %d", s.Age)
	return studentStr
}
